package com.mohamed.hany.weather_conditions.ui.main;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;

import com.bumptech.glide.Glide;
import com.mohamed.hany.weather_conditions.R;
import com.mohamed.hany.weather_conditions.database.CurrentWeatherEntry;
import com.mohamed.hany.weather_conditions.network.NetworkUtils;

import java.util.List;

public class CurrentWeatherAdaptor extends RecyclerView.Adapter<CurrentWeatherAdaptor.MyViewHolder> {
    private List<CurrentWeatherEntry> WeatherItems;
    private ItemsListener mItemsListener;
    public interface ItemsListener {
        void onItemClicked(int id);

    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        /**
         * ButterKnife Code
         **/
        @BindView(R.id.imageViewWeatherIcon)
        ImageView mImageView;
        @BindView(R.id.textViewCityName)
        TextView mCityNameView;
        @BindView(R.id.textViewWeatherDescription)
        TextView mDescriptionView;
        @BindView(R.id.high_temperature)
        TextView mHighView;
        @BindView(R.id.low_temperature)
        TextView mLowView;

        /**
         * ButterKnife Code
         **/


        public MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

    public CurrentWeatherAdaptor(List<CurrentWeatherEntry> WeatherItems, ItemsListener mItemsListener) {
        this.WeatherItems = WeatherItems;
        this.mItemsListener = mItemsListener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.city_list_item, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final CurrentWeatherEntry WeatherItem = WeatherItems.get(position);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mItemsListener.onItemClicked(position);
            }
        });

        holder.mCityNameView.setText(WeatherItem.getCity());
        holder.mDescriptionView.setText(WeatherItem.getCondition());
        holder.mHighView.setText(String.valueOf(WeatherItem.getMax()));
        holder.mLowView.setText(String.valueOf(WeatherItem.getMin()));

        Glide.with(holder.mImageView.getContext()).load(NetworkUtils.IMAGES_BASE_URL+WeatherItem.getWeatherIcon())
                .into(holder.mImageView);
    }

    @Override
    public int getItemCount() {
        return WeatherItems.size();
    }


}
