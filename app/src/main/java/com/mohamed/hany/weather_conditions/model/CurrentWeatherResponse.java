package com.mohamed.hany.weather_conditions.model;

import com.mohamed.hany.weather_conditions.database.CurrentWeatherEntry;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
public class CurrentWeatherResponse
{
    private int cnt;

    private ArrayList<CurrentWeather> list;

    public void setCnt(int cnt){
        this.cnt = cnt;
    }
    public int getCnt(){
        return this.cnt;
    }
    public void setList(ArrayList<CurrentWeather> list){
        this.list = list;
    }
    public ArrayList<CurrentWeather> getList(){
        return this.list;
    }


    private  List<CurrentWeatherEntry> mCurrentWeatherForecast=new ArrayList<>();
    private CurrentWeatherEntry  CurrentWeatherOpject;

    public List<CurrentWeatherEntry> getCurrentWeatherForecast() {

        for(int i=0;i<list.size();i++)
        {
            CurrentWeatherOpject=new CurrentWeatherEntry(list.get(i).getWeather().get(0).getIcon(),
                    list.get(i).getDt(),
                    list.get(i).getMain().getTemp_min(),
                    list.get(i).getMain().getTemp_max(),
                    list.get(i).getName(),
                    list.get(i).getWeather().get(0).getMain()
            );

            mCurrentWeatherForecast.add(CurrentWeatherOpject);
        }


        return mCurrentWeatherForecast;
    }

}

