package com.mohamed.hany.weather_conditions.database;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.mohamed.hany.weather_conditions.model.CurrentWeather;

import java.util.Date;
import java.util.List;

@Dao
public interface WeatherDao {

    /**
     * Selects all DailyWeatherEntry objects after a give city, inclusive into LiveData Opject
     *
     * @param city A from which to select future weather
     * @return LiveData list of all DailyWeatherEntry objects of one city
     */
    @Query("SELECT * FROM daily_weather WHERE city = :city")
    LiveData<List<DailyWeatherEntry>> getDailyWeather(String city);
    /**
     * Selects CurentWeatherEntry objects of all cities into LiveData Opject
     *
     * @return LiveData list of CurentWeatherEntry objects of all cities
     */
    @Query("SELECT * FROM current_weather")
    LiveData<List<CurrentWeatherEntry>> getCurrentWeather();


    /**
     * Inserts a list of  DailyWeatherEntry into the daily_weather table.
     *
     * @param DailyWeather A list of weather to insert
     */
    @Insert()
    void bulkInsertDaily(List<DailyWeatherEntry> DailyWeather);


    /**
     * Inserts a list of  CurrentWeatherEntry into the current_weather table.
     *
     * @param CurrentWeather A list of weather to insert
     */
    @Insert()
    void bulkInsertCurrent(List<CurrentWeatherEntry> CurrentWeather);


    //Deletes all weather data from daily_weather table.

    @Query("DELETE FROM daily_weather WHERE city = :city")
    void deleteDailyWeather(String city);

    //Deletes all weather data from current_weather table.

    @Query("DELETE FROM current_weather")
    void deleteCurrentWeather();


    @Query("SELECT COUNT(id) FROM daily_weather WHERE date >= :date & city = :city")
    int countAllFutureWeather(Long date,String city);


}
