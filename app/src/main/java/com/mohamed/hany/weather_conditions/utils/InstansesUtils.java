package com.mohamed.hany.weather_conditions.utils;

import android.content.Context;

import com.mohamed.hany.weather_conditions.database.WeatherDatabase;
import com.mohamed.hany.weather_conditions.database.WeatherRepository;
import com.mohamed.hany.weather_conditions.network.FetchNetworkData;
import com.mohamed.hany.weather_conditions.ui.city.CityViewModelFactory;
import com.mohamed.hany.weather_conditions.ui.main.MainViewModelFactory;

public class InstansesUtils {

    public static WeatherRepository provideRepository(Context context) {
        WeatherDatabase database = WeatherDatabase.getInstance(context.getApplicationContext());
        FetchNetworkData networkDataSource = FetchNetworkData.getInstance();
        return WeatherRepository.getInstance(database.weatherDao(), networkDataSource);
    }

    public static FetchNetworkData provideFetchNetworkData(Context context) {
        provideRepository(context.getApplicationContext());
        return FetchNetworkData.getInstance();
    }

    public static MainViewModelFactory provideMainActivityViewModelFactory(Context context) {
        WeatherRepository repository = provideRepository(context.getApplicationContext());
        return new MainViewModelFactory(repository);
    }

    public static CityViewModelFactory provideCityActivityViewModelFactory(Context context) {
        WeatherRepository repository = provideRepository(context.getApplicationContext());
        return new CityViewModelFactory(repository);
    }


}
