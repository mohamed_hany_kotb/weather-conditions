package com.mohamed.hany.weather_conditions.network;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.mohamed.hany.weather_conditions.database.CurrentWeatherEntry;
import com.mohamed.hany.weather_conditions.database.DailyWeatherEntry;
import com.mohamed.hany.weather_conditions.model.CurrentWeather;
import com.mohamed.hany.weather_conditions.model.CurrentWeatherResponse;
import com.mohamed.hany.weather_conditions.model.DailyWeatherResponse;

import java.io.IOException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Provides an API for doing all operations with the server data
 */

public class FetchNetworkData {

    private final MutableLiveData<List<CurrentWeatherEntry>> mDownloadedCurrentWeather;
    private final MutableLiveData<List<DailyWeatherEntry>> mDownloadedDailytWeather;

    // For Singleton instantiation
    private static FetchNetworkData sInstance;


    private FetchNetworkData() {
        mDownloadedCurrentWeather = new MutableLiveData<List<CurrentWeatherEntry>>();
        mDownloadedDailytWeather = new MutableLiveData<List<DailyWeatherEntry>>();
    }


    public LiveData<List<CurrentWeatherEntry>> getCurrentWeatherData() {
        loadCurrentWeather();

        return mDownloadedCurrentWeather;
    }

    public LiveData<List<DailyWeatherEntry>> getDailyWeatherData(String city) {
        loadDailyWeather(city);
        return mDownloadedDailytWeather;
    }

    /**
     * Get the singleton for this class
     */
    public static FetchNetworkData getInstance() {

        if (sInstance == null) {
                sInstance = new FetchNetworkData();
        }
        return sInstance;
    }

    public void loadCurrentWeather() {
        Log.d("hjj", "jkk");

         NetworkUtils.apiClient.getCurrentWeather(new Callback<CurrentWeatherResponse>() {
            @Override
            public void onResponse(Call<CurrentWeatherResponse> call, Response<CurrentWeatherResponse> response) {

                if (response.isSuccessful()) {

                    if (response.code() == 200) {

                        mDownloadedCurrentWeather.postValue(response.body().getCurrentWeatherForecast());


                    }
                }
            }
            @Override
            public void onFailure(Call<CurrentWeatherResponse> call, Throwable t) {
            }
        });
    }

    public void loadDailyWeather(String city) {

        NetworkUtils.apiClient.getDailytWeather(city,new Callback<DailyWeatherResponse>() {
            @Override
            public void onResponse(Call<DailyWeatherResponse> call, Response<DailyWeatherResponse> response) {

                if (response.isSuccessful()) {
                    if (response.code() == 200) {

                        mDownloadedDailytWeather.postValue(response.body().getDailyWeatherForecast());
                    }
                }
            }
            @Override
            public void onFailure(Call<DailyWeatherResponse> call, Throwable t) {

            }
        });

    }

}