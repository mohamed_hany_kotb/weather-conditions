package com.mohamed.hany.weather_conditions.ui.city;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;

import com.mohamed.hany.weather_conditions.database.WeatherRepository;

public class CityViewModelFactory extends ViewModelProvider.NewInstanceFactory {

    private final WeatherRepository mRepository;

    public CityViewModelFactory(WeatherRepository repository) {
        this.mRepository = repository;
    }

    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        return (T) new CityActivityViewModel(mRepository);
    }
}