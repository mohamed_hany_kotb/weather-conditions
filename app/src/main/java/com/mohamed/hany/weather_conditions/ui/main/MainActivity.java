package com.mohamed.hany.weather_conditions.ui.main;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.mohamed.hany.weather_conditions.R;
import com.mohamed.hany.weather_conditions.database.CurrentWeatherEntry;
import com.mohamed.hany.weather_conditions.ui.city.CityActivity;
import com.mohamed.hany.weather_conditions.utils.InstansesUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

public class MainActivity extends AppCompatActivity {

    private MainActivityViewModel mViewModel;

    private List<CurrentWeatherEntry> weatherItems;
    private CurrentWeatherAdaptor weatherAdaptor;


    android.support.v7.widget.RecyclerView mMyRecyclerView ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mMyRecyclerView = findViewById(R.id.recyclerview_cities);


        MainViewModelFactory mFactory = InstansesUtils.provideMainActivityViewModelFactory(this.getApplicationContext());
        mViewModel = ViewModelProviders.of(this, mFactory).get(MainActivityViewModel.class);

        weatherItems = new ArrayList<>();
        weatherAdaptor = new CurrentWeatherAdaptor(weatherItems, new CurrentWeatherAdaptor.ItemsListener() {
            @Override
            public void onItemClicked(int position) {

                Intent weatherCityIntent = new Intent(MainActivity.this, CityActivity.class);
                String city = weatherItems.get(position).getCity();
                weatherCityIntent.putExtra(CityActivity.City_Name_EXTRA, city);
                startActivity(weatherCityIntent);

            }
        });

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        mMyRecyclerView.setLayoutManager(mLayoutManager);
        mMyRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mMyRecyclerView.setAdapter(weatherAdaptor);

        mViewModel.getCurrentWeather().observe(this, weatherEntries -> {

            weatherItems.clear();
            weatherItems.addAll(weatherEntries);
            weatherAdaptor.notifyDataSetChanged();

        });

    }
}
