package com.mohamed.hany.weather_conditions.database;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.util.Log;

import com.mohamed.hany.weather_conditions.model.DailyWeather;
import com.mohamed.hany.weather_conditions.network.FetchNetworkData;
import com.mohamed.hany.weather_conditions.network.NetworkUtils;
import com.mohamed.hany.weather_conditions.utils.DateUtils;

import java.util.Date;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class WeatherRepository {


    // For Singleton instantiation
    private static final Object LOCK = new Object();
    private static WeatherRepository sInstance;
    private final WeatherDao mWeatherDao;
    private final FetchNetworkData mFetchNetworkData;
    private boolean mInitialized = false;

    private final Executor diskIO;

    private LiveData<List<CurrentWeatherEntry>> mCurrentWeatherData;
    private LiveData<List<DailyWeatherEntry>> mDailyWeatherData;


    private WeatherRepository(WeatherDao weatherDao,
                              FetchNetworkData fetchNetworkData
                              ) {
        mWeatherDao = weatherDao;
        mFetchNetworkData = fetchNetworkData;
        this.diskIO = Executors.newSingleThreadExecutor();

        // As long as the repository exists, observe the network LiveData.
        // If that LiveData changes, update the database.


        LiveData<List<CurrentWeatherEntry>> networkCurrentData = mFetchNetworkData.getCurrentWeatherData();

        Log.d("networkCurrentData","fd");

        networkCurrentData.observeForever(newDataFromNetwork -> {
            diskIO.execute(() -> {
                // Deletes old historical data
                mWeatherDao.deleteCurrentWeather();
                // Insert our new weather data into database
                mWeatherDao.bulkInsertCurrent(newDataFromNetwork);

                Log.d("networkCurrentData", newDataFromNetwork.size()+"fd");

                mCurrentWeatherData=networkCurrentData;
            });
        });


    }

    public synchronized static WeatherRepository getInstance(
            WeatherDao weatherDao, FetchNetworkData fetchNetworkData )
    {
        if (sInstance == null) {
            synchronized (LOCK) {
                sInstance = new WeatherRepository(weatherDao, fetchNetworkData );
            }
        }
        return sInstance;
    }

    /**
     * Database related operations
     **/
    public LiveData<List<CurrentWeatherEntry>> getCurrentWeather() {
        initializeCurrentWeatherData();
        mCurrentWeatherData = mWeatherDao.getCurrentWeather();
        Log.d("lkjk","getCurrentWeather");

        return mCurrentWeatherData;

    }

    public LiveData<List<DailyWeatherEntry>> getDailyWeather(String city) {
        initializeDailyWeatherData(city);
        mDailyWeatherData = mWeatherDao.getDailyWeather(city);
        return mDailyWeatherData;
    }


    private synchronized void initializeCurrentWeatherData() {
        diskIO.execute(() -> {

            mFetchNetworkData.getCurrentWeatherData();
        });
       }


    private synchronized void initializeDailyWeatherData(String city) {

        diskIO.execute(() -> {
            if (isFetchNeeded(city)) {
                LiveData<List<DailyWeatherEntry>> networkDailyData = mFetchNetworkData.getDailyWeatherData(city);

                networkDailyData.observeForever(newDataFromNetwork -> {
                    diskIO.execute(() -> {

                        // Deletes old historical data
                        mWeatherDao.deleteDailyWeather(city);
                        // Insert our new weather data into database
                        mWeatherDao.bulkInsertDaily(newDataFromNetwork);

                        mDailyWeatherData = networkDailyData;
                    });
                });
            }
        });

    }


    /**
     * Checks if there are new day to update local data.
     *
     * @return Whether a fetch is needed
     */
    private boolean isFetchNeeded(String city) {
       Long today = DateUtils.getNormalizedUtcMsForToday();
       int count = mWeatherDao.countAllFutureWeather(today,city);
       return (count < Integer.valueOf(NetworkUtils.NUM_DAY));
    }


}