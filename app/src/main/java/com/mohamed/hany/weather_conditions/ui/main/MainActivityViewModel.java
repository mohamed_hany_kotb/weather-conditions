package com.mohamed.hany.weather_conditions.ui.main;


import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;

import com.mohamed.hany.weather_conditions.database.CurrentWeatherEntry;
import com.mohamed.hany.weather_conditions.database.WeatherRepository;

import java.util.List;

class MainActivityViewModel extends ViewModel {

    private final WeatherRepository mRepository;

    public MainActivityViewModel(WeatherRepository repository) {
        mRepository = repository;
    }

    public LiveData<List<CurrentWeatherEntry>> getCurrentWeather() {
        return mRepository.getCurrentWeather();
    }


}
