package com.mohamed.hany.weather_conditions.model;

import java.util.ArrayList;
import java.util.List;
public class CurrentWeather
{
    private Coord coord;

    private Sys sys;

    private List<Weather> weather;

    private Main main;


    private Wind wind;

    private Clouds clouds;

    private Long dt;

    private int id;

    private String name;

    public void setCoord(Coord coord){
        this.coord = coord;
    }
    public Coord getCoord(){
        return this.coord;
    }
    public void setSys(Sys sys){
        this.sys = sys;
    }
    public Sys getSys(){
        return this.sys;
    }
    public void setWeather(List<Weather> weather){
        this.weather = weather;
    }
    public List<Weather> getWeather(){
        return this.weather;
    }
    public void setMain(Main main){
        this.main = main;
    }
    public Main getMain(){
        return this.main;
    }
    public void setWind(Wind wind){
        this.wind = wind;
    }
    public Wind getWind(){
        return this.wind;
    }
    public void setClouds(Clouds clouds){
        this.clouds = clouds;
    }
    public Clouds getClouds(){
        return this.clouds;
    }
    public void setDt(Long dt){
        this.dt = dt;
    }
    public Long getDt(){
        return this.dt;
    }
    public void setId(int id){
        this.id = id;
    }
    public int getId(){
        return this.id;
    }
    public void setName(String name){
        this.name = name;
    }
    public String getName(){
        return this.name;
    }
}