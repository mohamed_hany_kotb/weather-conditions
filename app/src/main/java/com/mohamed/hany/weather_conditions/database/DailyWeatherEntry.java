package com.mohamed.hany.weather_conditions.database;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;

import java.util.Date;

@Entity(tableName = "daily_weather")
public class DailyWeatherEntry {

    @PrimaryKey(autoGenerate = true)
    private int id;
    private String weatherIcon;
    private String city;
    private String condition;
    private double min;
    private double max;
    private Long date;


    /**
     * this constructor used to converts json data to DailyWeatherEntry objects.
     * @param weatherIconId Image id for weather
     * @param date Date of weather
     * @param min Min temperature
     * @param max Max temperature
     * @param city city name
     * @param condition weather condition descreption
     */
    @Ignore
    public DailyWeatherEntry(String weatherIconId, Long date, double min, double max, String city, String condition) {
        this.weatherIcon = weatherIconId;
        this.date = date;
        this.min = min;
        this.max = max;
        this.city = city;
        this.condition = condition;
    }

    // Constructor used by Room to create DailyWeatherEntries
    public DailyWeatherEntry(int id ,String weatherIcon, Long date, double min, double max, String city, String condition) {
        this.id=id;
        this.weatherIcon = weatherIcon;
        this.date = date;
        this.min = min;
        this.max = max;
        this.city = city;
        this.condition = condition;
    }


    public Long getDate() {
        return date;
    }

    public int getId() {
        return id;
    }

    public String getCity() {
        return city;
    }

    public String getCondition() {
        return condition;
    }

    public double getMax() {
        return max;
    }

    public double getMin() {
        return min;
    }

    public String getWeatherIcon() {
        return weatherIcon;
    }

}
