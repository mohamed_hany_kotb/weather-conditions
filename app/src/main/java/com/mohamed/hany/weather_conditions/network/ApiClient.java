package com.mohamed.hany.weather_conditions.network;

import android.content.Context;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import com.mohamed.hany.weather_conditions.model.CurrentWeatherResponse;
import com.mohamed.hany.weather_conditions.model.DailyWeatherResponse;


public class ApiClient {


    private static Retrofit retrofit = null;


    HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();


    public static Retrofit getClient() {
        if (retrofit == null) {
            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            retrofit = new Retrofit.Builder()
                    .baseUrl(NetworkUtils.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(new OkHttpClient.Builder().addInterceptor(interceptor)
                            .connectTimeout(10, TimeUnit.SECONDS)
                            .readTimeout(10, TimeUnit.SECONDS)
                            .writeTimeout(10, TimeUnit.SECONDS)
                            .build())
                    .build();
        }
        return retrofit;
    }


    public void getCurrentWeather(Callback<CurrentWeatherResponse> voidCallback) {
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);
        Call<CurrentWeatherResponse> call = apiService.getCurrentWeather(NetworkUtils.CITIES_ID,NetworkUtils.UNIT,NetworkUtils.API_KEY);
        call.enqueue(voidCallback);
    }

    public void getDailytWeather(String city_name, Callback<DailyWeatherResponse> voidCallback) {
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);
        Call<DailyWeatherResponse> call = apiService.getDailytWeather(city_name,NetworkUtils.UNIT,NetworkUtils.NUM_DAY,NetworkUtils.API_KEY);
        call.enqueue(voidCallback);
    }



}
