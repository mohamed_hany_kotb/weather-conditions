package com.mohamed.hany.weather_conditions.model;
import com.mohamed.hany.weather_conditions.database.CurrentWeatherEntry;
import com.mohamed.hany.weather_conditions.database.DailyWeatherEntry;

import java.util.ArrayList;
import java.util.List;
public class DailyWeatherResponse
{
    private City city;

    private String cod;

    private double message;

    private int cnt;

    private ArrayList<DailyWeather> list;

    public void setCity(City city){
        this.city = city;
    }
    public City getCity(){
        return this.city;
    }
    public void setCod(String cod){
        this.cod = cod;
    }
    public String getCod(){
        return this.cod;
    }
    public void setMessage(double message){
        this.message = message;
    }
    public double getMessage(){
        return this.message;
    }
    public void setCnt(int cnt){
        this.cnt = cnt;
    }
    public int getCnt(){
        return this.cnt;
    }
    public void setList(ArrayList<DailyWeather> list){
        this.list = list;
    }
    public ArrayList<DailyWeather> getList(){
        return this.list;
    }



    private  List<DailyWeatherEntry> mDailyWeatherForecast = new ArrayList<>() ;

    private DailyWeatherEntry  mDailyWeatherOpject;

    public List<DailyWeatherEntry> getDailyWeatherForecast() {

        for(int i=0;i<list.size();i++)
        {
            mDailyWeatherOpject=new DailyWeatherEntry(list.get(i).getWeather().get(0).getIcon(),
                    list.get(i).getDt(),
                    list.get(i).getTemp().getMin(),
                    list.get(i).getTemp().getMax(),
                    city.getName(),
                    list.get(i).getWeather().get(0).getMain()
            );

            mDailyWeatherForecast.add(mDailyWeatherOpject);
        }

        return mDailyWeatherForecast;
    }

}