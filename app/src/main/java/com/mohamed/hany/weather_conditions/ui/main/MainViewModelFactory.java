package com.mohamed.hany.weather_conditions.ui.main;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;

import com.mohamed.hany.weather_conditions.database.WeatherRepository;


 // Factory class for pass Repository to View Model within constructor

public class MainViewModelFactory extends ViewModelProvider.NewInstanceFactory {

    private final WeatherRepository mRepository;

    public MainViewModelFactory(WeatherRepository repository) {
        this.mRepository = repository;
    }

    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        return (T) new MainActivityViewModel(mRepository);
    }
}