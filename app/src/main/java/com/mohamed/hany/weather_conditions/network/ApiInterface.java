package com.mohamed.hany.weather_conditions.network;

import com.mohamed.hany.weather_conditions.model.CurrentWeatherResponse;
import com.mohamed.hany.weather_conditions.model.DailyWeatherResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ApiInterface {


    @GET("/data/2.5/group")
    public Call<CurrentWeatherResponse> getCurrentWeather(@Query("id")String cities, @Query("units")String unit, @Query("appid")String key );

    @GET("/data/2.5/forecast/daily")
    public Call<DailyWeatherResponse> getDailytWeather(@Query("q")String city_name, @Query("units")String unit, @Query("cnt")String num_day, @Query("appid")String key );


}
