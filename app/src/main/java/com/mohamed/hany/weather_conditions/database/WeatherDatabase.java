package com.mohamed.hany.weather_conditions.database;


import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.TypeConverters;
import android.content.Context;
import android.util.Log;

@Database(entities = {DailyWeatherEntry.class,CurrentWeatherEntry.class}, version = 1)
public abstract class WeatherDatabase extends RoomDatabase {

    private static final String LOG_TAG = WeatherDatabase.class.getSimpleName();
    private static final String DATABASE_NAME = "weather";

    // For Singleton instantiation
    private static final Object LOCK = new Object();
    private static WeatherDatabase sInstance;

    public static WeatherDatabase getInstance(Context context) {
        if (sInstance == null) {
            synchronized (LOCK) {
                sInstance = Room.databaseBuilder(context.getApplicationContext(),
                        WeatherDatabase.class, WeatherDatabase.DATABASE_NAME).build();
            }
        }
        return sInstance;
    }

    // The associated DAOs for the database
    public abstract WeatherDao weatherDao();
}
