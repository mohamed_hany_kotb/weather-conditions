package com.mohamed.hany.weather_conditions.ui.city;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.mohamed.hany.weather_conditions.R;
import com.mohamed.hany.weather_conditions.database.CurrentWeatherEntry;
import com.mohamed.hany.weather_conditions.database.DailyWeatherEntry;
import com.mohamed.hany.weather_conditions.network.NetworkUtils;
import com.mohamed.hany.weather_conditions.utils.DateUtils;

import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CityWeatherAdaptor extends RecyclerView.Adapter<CityWeatherAdaptor.MyViewHolder> {
    private List<DailyWeatherEntry> WeatherItems;
    private CityWeatherAdaptor.ItemsListener mItemsListener;
    public interface ItemsListener {
        void onItemClicked(int id);

    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        /**
         * ButterKnife Code
         **/
        @BindView(R.id.imageViewWeatherIcon)
        ImageView mImageView;
        @BindView(R.id.textViewDate)
        TextView mDateView;
        @BindView(R.id.textViewWeatherDescription)
        TextView mDescriptionView;
        @BindView(R.id.high_temperature)
        TextView mHighView;
        @BindView(R.id.low_temperature)
        TextView mLowView;

        /**
         * ButterKnife Code
         **/


        public MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

    public CityWeatherAdaptor(List<DailyWeatherEntry> WeatherItems, CityWeatherAdaptor.ItemsListener mItemsListener) {
        this.WeatherItems = WeatherItems;
        this.mItemsListener = mItemsListener;
    }

    @Override
    public CityWeatherAdaptor.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.weather_list_item, parent, false);
        return new CityWeatherAdaptor.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final CityWeatherAdaptor.MyViewHolder holder, final int position) {
        final DailyWeatherEntry WeatherItem = WeatherItems.get(position);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mItemsListener.onItemClicked(position);
            }
        });

        holder.mDateView.setText(DateUtils.getStringDate(WeatherItem.getDate()));
        holder.mDescriptionView.setText(WeatherItem.getCondition());
        holder.mHighView.setText(String.valueOf(WeatherItem.getMax()));
        holder.mLowView.setText(String.valueOf(WeatherItem.getMin()));

        Glide.with(holder.mImageView.getContext()).load(NetworkUtils.IMAGES_BASE_URL+WeatherItem.getWeatherIcon())
                .into(holder.mImageView);
    }

    @Override
    public int getItemCount() {
        return WeatherItems.size();
    }


}
