package com.mohamed.hany.weather_conditions.ui.city;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;

import com.mohamed.hany.weather_conditions.database.CurrentWeatherEntry;
import com.mohamed.hany.weather_conditions.database.DailyWeatherEntry;
import com.mohamed.hany.weather_conditions.database.WeatherRepository;

import java.util.List;

public class CityActivityViewModel extends ViewModel {

    private final WeatherRepository mRepository;

    public CityActivityViewModel(WeatherRepository repository) {
        mRepository = repository;
    }

    public LiveData<List<DailyWeatherEntry>> getDailyWeather(String city ) {
        return mRepository.getDailyWeather(city);
    }


}