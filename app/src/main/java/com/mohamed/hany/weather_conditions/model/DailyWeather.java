package com.mohamed.hany.weather_conditions.model;

import java.util.ArrayList;
import java.util.List;
public class DailyWeather
{
    private Long dt;

    private Temp temp;

    private double pressure;

    private int humidity;

    private List<Weather> weather;

    private double speed;

    private int deg;

    private int clouds;

    public void setDt(Long dt){
        this.dt = dt;
    }
    public Long getDt(){
        return this.dt;
    }
    public void setTemp(Temp temp){
        this.temp = temp;
    }
    public Temp getTemp(){
        return this.temp;
    }
    public void setPressure(double pressure){
        this.pressure = pressure;
    }
    public double getPressure(){
        return this.pressure;
    }
    public void setHumidity(int humidity){
        this.humidity = humidity;
    }
    public int getHumidity(){
        return this.humidity;
    }
    public void setWeather(List<Weather> weather){
        this.weather = weather;
    }
    public List<Weather> getWeather(){
        return this.weather;
    }
    public void setSpeed(double speed){
        this.speed = speed;
    }
    public double getSpeed(){
        return this.speed;
    }
    public void setDeg(int deg){
        this.deg = deg;
    }
    public int getDeg(){
        return this.deg;
    }
    public void setClouds(int clouds){
        this.clouds = clouds;
    }
    public int getClouds(){
        return this.clouds;
    }
}
