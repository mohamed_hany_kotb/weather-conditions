package com.mohamed.hany.weather_conditions.ui.city;

import android.arch.lifecycle.ViewModelProviders;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.mohamed.hany.weather_conditions.R;
import com.mohamed.hany.weather_conditions.database.CurrentWeatherEntry;
import com.mohamed.hany.weather_conditions.database.DailyWeatherEntry;
import com.mohamed.hany.weather_conditions.ui.main.CurrentWeatherAdaptor;
import com.mohamed.hany.weather_conditions.ui.main.MainViewModelFactory;
import com.mohamed.hany.weather_conditions.utils.InstansesUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;


public class CityActivity extends AppCompatActivity {

    public static String City_Name_EXTRA = "";

    private CityActivityViewModel mViewModel;

    private List<DailyWeatherEntry> weatherItems;
    private CityWeatherAdaptor weatherAdaptor;


    android.support.v7.widget.RecyclerView mMyRecyclerView ;

    //test1
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_city);

        mMyRecyclerView = findViewById(R.id.recyclerview_days);


        CityViewModelFactory mFactory = InstansesUtils.provideCityActivityViewModelFactory(this.getApplicationContext());
        mViewModel = ViewModelProviders.of(this, mFactory).get(CityActivityViewModel.class);

        weatherItems = new ArrayList<>();
        weatherAdaptor = new CityWeatherAdaptor(weatherItems, new CityWeatherAdaptor.ItemsListener() {
            @Override
            public void onItemClicked(int id) {

            }
        });

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        mMyRecyclerView.setLayoutManager(mLayoutManager);
        mMyRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mMyRecyclerView.setAdapter(weatherAdaptor);

        mViewModel.getDailyWeather(getIntent().getStringExtra(City_Name_EXTRA)).observe(this, weatherEntries -> {

            weatherItems.clear();
            weatherItems.addAll(weatherEntries);
            weatherAdaptor.notifyDataSetChanged();

        });

    }
}
