package com.mohamed.hany.weather_conditions.model;

public class Sys
{
    private int type;

    private int id;

    private double message;

    private String country;

    private long sunrise;

    private long sunset;

    public void setType(int type){
        this.type = type;
    }
    public int getType(){
        return this.type;
    }
    public void setId(int id){
        this.id = id;
    }
    public int getId(){
        return this.id;
    }
    public void setMessage(double message){
        this.message = message;
    }
    public double getMessage(){
        return this.message;
    }
    public void setCountry(String country){
        this.country = country;
    }
    public String getCountry(){
        return this.country;
    }
    public void setSunrise(long sunrise){
        this.sunrise = sunrise;
    }
    public long getSunrise(){
        return this.sunrise;
    }
    public void setSunset(long sunset){
        this.sunset = sunset;
    }
    public long getSunset(){
        return this.sunset;
    }
}